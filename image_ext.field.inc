<?php

/**
 * @file
 * Implement an image extended field, based on the image field which is based on the file module's file field.
 */

/**
 * Implements hook_field_info().
 */
function img_ext_field_info() {
  return array(
    'img_ext' => array(
      'label' => t('Image Extended'),
      'description' => t('This field stores the ID of an image extended file as an integer value.'),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
        'default_image' => 0,
      ),
      'instance_settings' => array(
        'file_extensions' => 'png gif jpg jpeg pdf svg tiff tif eps',
        'file_directory' => '',
        'max_filesize' => '',
        'alt_field' => 0,
        'title_field' => 0,
        'max_resolution' => '',
        'img_ext_max_active'=> 0,
        'min_resolution' => '',
        'img_ext_min_active' => 0,
        'default_image' => 0,
      ),
      'default_widget' => 'img_ext_img_ext_widget',
      'default_formatter' => 'img_ext',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function img_ext_field_widget_info() {
  return array(
    'img_ext_img_ext_widget' => array(
      'label' => t('Image Extended'),
      'field types' => array('img_ext'),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function img_ext_field_settings_form($field, $instance) {
  $form = image_field_settings_form($field, $instance);
  return $form;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function img_ext_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];
  $form = image_field_instance_settings_form($field, $instance);
  $form['file_extensions']['#description'] = t('Separate extensions with a space or '
      . 'comma and do not include the leading dot.').'<b> '. t("Be aware that using "
          . "svg is very dangerous if you don't know the uploader and you can trust him. "
          . "In doubt delete this extension!").'</b>';
  $form['img_ext_max_active'] = array(
    '#type' => 'checkbox',
    '#default_value' => $settings['img_ext_max_active'],
    '#title' => t('Max resolution for non default image extensions.'),
    '#description' => t('Shows errors if size is to big. Usually not needed because pdf or svg are documents.'),
    '#weight' => 4.11,
  );
  $form['img_ext_min_active'] = array(
    '#type' => 'checkbox',
    '#default_value' => $settings['img_ext_min_active'],
    '#title' => t('Min resolution for non default image extensions.'),
    '#description' => t('Shows errors if size is to small.'),
    '#weight' => 4.21,
  );
  return $form;
  //return system_settings_form($form);
}

/**
 * Implements hook_field_load().
 * 
 * hook_field_load is more a callback than a real hook 
 * @see https://www.drupal.org/node/702586
 * Loads the files from the files table and if the file does not exist, mark the entire item as empty.
 */
function img_ext_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  file_field_load($entity_type, $entities, $field, $instances, $langcode, $items, $age);
}

/**
 * Implements hook_field_insert().
 */
function img_ext_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_insert($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_update().
 */
function img_ext_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {

  file_field_update($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete().
 */
function img_ext_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  file_field_delete($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_delete_revision().
 */
function img_ext_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {

  file_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, $items);
}
/**
 * Implements hook_field_is_empty().
 */
function img_ext_field_is_empty($item, $field) {

  return file_field_is_empty($item, $field);
}

/**
 * Implements hook_field_formatter_info().
 * 
 * tells Drupal what fields it applies to and what settings are available
 */
function img_ext_field_formatter_info() {
  $formatters = array(
    'img_ext' => array(
      'label' => t('Image Extended'),
      'field types' => array('img_ext'),
      'settings' => array('image_style' => '', 'image_link' => ''),
      'fallback_formatter' => 'file_default',
    ),
  );

  return $formatters;
}
/**
 * Implements hook_field_formatter_info_alter().
 */
function img_ext_field_formatter_info_alter(&$info) {
  $info['img_ext']['settings'] += $info['image']['settings'];
}

/**
 * Implements hook_field_formatter_settings_form().
 * 
 * tell Drupal how to generate the form that collects the options
 */
function img_ext_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

  $element = image_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 * 
 * displays the chosen settings on the 'Manage Display' page
 */
function img_ext_field_formatter_settings_summary($field, $instance, $view_mode) {
  
  
  

  $string = image_field_formatter_settings_summary($field, $instance, $view_mode);
    
  return $string;
}

/**
 * Implements hook_field_formatter_view().
 * 
 * This is the hook where we actually do the formatting
 */
function img_ext_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  
  
  
  
  $element = array();

  // Check if the formatter involves a link.
  if ($display['settings']['image_link'] == 'content') {
    $uri = entity_uri($entity_type, $entity);
  }
  elseif ($display['settings']['image_link'] == 'file') {
    $link_file = TRUE;
  }
  
  foreach ($items as $delta => $item) {
    if (isset($link_file)) {
      $uri = array(
        'path' => file_create_url($item['uri']),
        'options' => array(
          'attributes' => array(
            'target' => "_blank",
            ),
        ),
      );
    }
    $element[$delta] = array(
      '#theme' => 'img_ext_formatter',
      '#item' => $item,
      '#image_style' => $display['settings']['image_style'],
      '#path' => isset($uri) ? $uri : '',
    );
  }

  return $element;
}

/**
 * Implements hook_field_prepare_view().
 */
function img_ext_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  
  
  
  // If there are no files specified at all, use the default.
  foreach ($entities as $id => $entity) {
    if (empty($items[$id])) {
      $fid = 0;
      // Use the default for the instance if one is available.
      if (!empty($instances[$id]['settings']['default_image'])) {
        $fid = $instances[$id]['settings']['default_image'];
      }
      // Otherwise, use the default for the field.
      elseif (!empty($field['settings']['default_image'])) {
        $fid = $field['settings']['default_image'];
      }

      // Add the default image if one is found.
      if ($fid && ($file = file_load($fid))) {
        $items[$id][0] = (array) $file + array(
          'is_default' => TRUE,
          'alt' => '',
          'title' => '',
        );
      }
    }
  }
}

/**
 * Implements hook_field_presave().
 * 
 * hook_field_load is more a callback than a real hook 
 * @see https://api.drupal.org/comment/24658#comment-24658
 */
function img_ext_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  
  
  file_field_presave($entity_type, $entity, $field, $instance, $langcode, $items);

  // Determine the dimensions if necessary.
  foreach ($items as &$item) {
    if (!isset($item['width']) || !isset($item['height'])) {
      $info = image_get_info(file_load($item['fid'])->uri);
      if (is_array($info)) {
        $item['width'] = $info['width'];
        $item['height'] = $info['height'];
      }
    }
  }
}

/**
 * Implements hook_field_widget_form().
 * 
 * change the validation and add a process function to theme the form
 */
function img_ext_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  
  // taken from image_field_widget_form():
  
  // Add display_field setting to field because file_field_widget_form() assumes it is set.
  $field['settings']['display_field'] = 0;

  $elements = file_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  //dpm($elements);
  $settings = $instance['settings'];
  
    
  foreach (element_children($elements) as $delta) {
    // For hook_validate() add max_resolution and min_resolution to $_Session.
  // For img_ext_get_pdf_validation() set Session "IMG EXT MAX DOC" "IMG EXT MIN DOC"
  if ($settings['max_resolution'] || $settings['min_resolution']) {
    if(array_key_exists('max_resolution', $settings)  && $settings['max_resolution'] !== ''){
      img_ext_session_set('IMG EXT MAX RES', $settings['max_resolution']);
      
      if(array_key_exists('img_ext_max_active', $settings)  && $settings['img_ext_max_active'] == 1){
        img_ext_session_set('IMG EXT MAX DOC', 1);
      }
      else img_ext_session_del('IMG EXT MAX DOC');
    }
    else{
      img_ext_session_del('IMG EXT MAX RES');
      img_ext_session_del('IMG EXT MAX DOC');
    }
    
    if(array_key_exists('min_resolution', $settings) && $settings['min_resolution'] !== ''){
      img_ext_session_set('IMG EXT MIN RES', $settings['min_resolution']);
      
      if(array_key_exists('img_ext_min_active', $settings)  && $settings['img_ext_min_active'] == 1){
        img_ext_session_set('IMG EXT MIN DOC', 1);
      }
      else img_ext_session_del('IMG EXT MIN DOC');
    }
    else{
      img_ext_session_del('IMG EXT MIN RES');
      img_ext_session_del('IMG EXT MIN DOC');
    }
  }

  else{
    img_ext_session_del('IMG EXT MAX RES');
    img_ext_session_del('IMG EXT MIN RES');
    img_ext_session_del('IMG EXT MAX DOC');
    img_ext_session_del('IMG EXT MIN DOC');
  }
    
    // Add upload validators for different extensions.
    $elements[$delta]['#upload_validators']['file_validate_extensions'][0] = $instance['settings']['file_extensions'];
    // Add all extra functionality provided by the img_ext widget.
    $elements[$delta]['#process'][] = 'img_ext_field_widget_process';
  }

  if ($field['cardinality'] == 1) {
    // If there's only one field, return it as delta 0.
    if (empty($elements[0]['#default_value']['fid'])) {
      $elements[0]['#description'] = theme('file_upload_help', array('description' => field_filter_xss($instance['description']), 'upload_validators' => $elements[0]['#upload_validators']));
    }
  }
  else {
    $elements['#file_upload_description'] = theme('file_upload_help', array('upload_validators' => $elements[0]['#upload_validators']));
  }
  
  return $elements;
}

/**
 * An element #process callback for the image_image field type.
 *
 * Expands the image_image type to include the alt and title fields. 
 */
function img_ext_field_widget_process($element, &$form_state, $form) {

  $item = $element['#value'];
  $item['fid'] = $element['fid']['#value'];

  $instance = field_widget_instance($element, $form_state);
  $settings = $instance['settings'];
  $widget_settings = $instance['widget']['settings'];

  $element['#theme'] = 'img_ext_widget';
  $element['#attached']['css'][] = drupal_get_path('module', 'image') . '/image.css';

  // Add the image preview.
  if ($element['#file'] && $widget_settings['preview_image_style']) {
    $variables = array(
      'style_name' => $widget_settings['preview_image_style'],
      'path' => $element['#file']->uri,
    );

    // Determine image dimensions.
    if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
      $variables['width'] = $element['#value']['width'];
      $variables['height'] = $element['#value']['height'];
    }
    else {
      $info = image_get_info($element['#file']->uri);

      if (is_array($info)) {
        $variables['width'] = $info['width'];
        $variables['height'] = $info['height'];
      }
      
      // this will be changed because the db table columns  can't be NULL and must be saved.
      else {
        $variables['width'] = $variables['height'] = 100;
      }
    }
    
    // Add the very first time the widget is called before a $_Session is set in 
    // hook_file_validate() the preview can't be called for non default image 
    // previews, so we have to set it here 
    // @see img_ext_file_validate()
    
    // Get the extension of the file as uppercase.
    $extension = substr(strrchr($element['#file']->filename,'.'),1);
    $extension_upper = strtoupper($extension);
    
    // String with all non default image extensions
    $non_default_extensions = "tiff tif eps svg pdf";
    
    // Set session variable that gets used in theme_img_ext_style()
    if(strpos($non_default_extensions,$extension) !== FALSE){
      img_ext_session_set("IMG EXT",$extension_upper);
    }
    else  img_ext_session_del ("IMG EXT");

    $element['preview'] = array(
      '#type' => 'markup',
      '#markup' => theme('img_ext_style', $variables),
    );

    // Store the dimensions in the form so the file doesn't have to be accessed
    // again. This is important for remote files.
    $element['width'] = array(
      '#type' => 'hidden',
      '#value' => $variables['width'],
    );
    $element['height'] = array(
      '#type' => 'hidden',
      '#value' => $variables['height'],
    );
  }

  // Add the additional alt and title fields.
  $element['alt'] = array(
    '#title' => t('Alternate text'),
    '#type' => 'textfield',
    '#default_value' => isset($item['alt']) ? $item['alt'] : '',
    '#description' => t('This text will be used by screen readers, search engines, or when the image cannot be loaded.'),
    // @see http://www.gawds.org/show.php?contentid=28
    '#maxlength' => 512,
    '#weight' => -2,
    '#access' => (bool) $item['fid'] && $settings['alt_field'],
  );
  $element['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($item['title']) ? $item['title'] : '',
    '#description' => t('The title is used as a tool tip when the user hovers the mouse over the image.'),
    '#maxlength' => 1024,
    '#weight' => -1,
    '#access' => (bool) $item['fid'] && $settings['title_field'],
  );
  //dpm($element,"Image EXT Field inc- img_ext_field_widget_process - Wert nach pdf speichern");
  return $element;

}

/**
 * Implements hook_field_widget_settings_form().
 */
function img_ext_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  // Use the file widget settings form.
  $form = file_field_widget_settings_form($field, $instance);

  $form['preview_image_style'] = array(
    '#title' => t('Preview image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE, PASS_THROUGH),
    '#empty_option' => '<' . t('no preview') . '>',
    '#default_value' => $settings['preview_image_style'],
    '#description' => t('The preview image will be shown while editing the content.'),
    '#weight' => 15,
  );

  return $form;
}


/**
 * Returns HTML for an image field widget.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element representing the image field widget.
 *
 * @ingroup themeable
 */
function theme_img_ext_widget($variables) {

  $element = $variables['element'];
  $output = '';
  $output .= '<div class="img_ext-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="image-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="image-widget-data">';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}


/**
 * Returns HTML for an image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - image_style: An optional image style.
 *   - path: An array containing the link 'path' and link 'options'.
 *
 * @ingroup themeable
 */
function theme_img_ext_formatter($variables) {
  
  $item = $variables['item'];
  //dpm($item);
  
  $matches = array();
  
  // Creates $uri from pdf to the appropriate jpg.
  if(strpos($item['uri'], 'pdf') !== false){
    
    preg_match('/(?<=\/\/).*(?=\.pdf)/', $item['uri'], $matches);
    $filename_without_pdf_extension = $matches[0];
    $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf.jpg";
    $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf.jpg" );
    
    $image = array(
      'path' => $new_image_scheme,
    );
    
    // Creates new image path and size for variables.
    $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);    
    $image['width'] = $details['width'];
    $image['height'] = $details['height'];
  }
  
  // Creates $uri from svg to the appropriate jpg.
  elseif(strpos($item['uri'], 'svg') !== false){
    
    preg_match('/(?<=\/\/).*(?=\.svg)/', $item['uri'], $matches);
    $filename_without_svg_extension = $matches[0];
    $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_svg_extension" . "_svg.jpg";
    $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_svg_extension" . "_svg.jpg" );
    
    $image = array(
      'path' => $new_image_scheme,
    );
    
    // Creates new image path and size for variables.
    $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
    $image['width'] = $details['width'];
    $image['height'] = $details['height'];
  }
  
  // Creates $uri from eps to the appropriate jpg.
  elseif(strpos($item['uri'], 'eps') !== false){
    
    preg_match('/(?<=\/\/).*(?=\.eps)/', $item['uri'], $matches);
    $filename_without_eps_extension = $matches[0];
    $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_eps_extension" . "_eps.jpg";
    $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_eps_extension" . "_eps.jpg" );
    
    $image = array(
      'path' => $new_image_scheme,
    );
    
    // Creates new image path and size for variables.
    $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
    $image['width'] = $details['width'];
    $image['height'] = $details['height'];
  }
  
  // Creates $uri from tiff to the appropriate jpg.
  elseif(strpos($item['uri'], '.tif') !== false){
    
    preg_match('/(?<=\/\/).*(?=\.tif)/', $item['uri'], $matches);
    $filename_without_tiff_extension = $matches[0];
    $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_tiff_extension" . "_tiff.jpg";
    $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_tiff_extension" . "_tiff.jpg" );
    
    $image = array(
      'path' => $new_image_scheme,
    );
    
    // Creates new image path and size for variables.
    $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
    $image['width'] = $details['width'];
    $image['height'] = $details['height'];
  }
  
  else{
    $image = array(
      'path' => $item['uri'],
    );
    if (isset($item['width']) && isset($item['height'])) {
      $image['width'] = $item['width'];
      $image['height'] = $item['height'];
    }
  }
  

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }  

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  if ($variables['image_style']) {
    $image['style_name'] = $variables['image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  // The link path and link options are both optional, but for the options to be
  // processed, the link path must at least be an empty string.
  if (isset($variables['path']['path'])) {
    $path = $variables['path']['path'];
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
  }

  return $output;
}

/****************  pdf and svg section  **************/

/**
 * create variable img_ext_identify if not already set
 * this function is called when field_instance_settings for img_ext fields are
 * saved
 */
function img_ext_variable_identify(){
  if(!variable_get("img_ext_identify")){    
    // replace the string convert with identify and save a new variable which we
    // need for shell execution later
    
    if(variable_get("imagemagick_convert") && variable_get("imagemagick_convert" !== '')){
      variable_set("img_ext_identify",str_replace('convert','identify', variable_get("imagemagick_convert")));
    }
    else{
      drupal_set_message(t('Unfortunately imagemagick seems not to be set as the activated '
      . 'toolkit, please go to <a href="@admin">Image Toolkit</a>.',
      array('@admin'=> url('/admin/config/media/image-toolkit'))),'error');
    }
  }
}

/**
 * update variable img_ext_identify
 * this function is called when image toolkit config is changed
 */
function img_ext_variable_identify_update(){
  
  // replace the string convert with identify and update variable which we
  // need for shell execution later
  $im = variable_get("imagemagick_convert");
  if($im !== ''){
    variable_set("img_ext_identify",str_replace('convert','identify', variable_get("imagemagick_convert")));
  }
  else{
    drupal_set_message(t('Unfortunately imagemagick seems not to be set as the activated '
    . 'toolkit, please go to <a href="@admin">Image Toolkit</a>.',
    array('@admin'=> url('/admin/config/media/image-toolkit'))),'error');
  }
}




function img_ext_convert($variables){
  $variables_new = array();
  // Create directory from path.
  $imgext_dir = file_default_scheme() . '://' . 'imgext';
  if(!is_dir($imgext_dir)){
    _img_ext_prepare_filesystem($imgext_dir);
  }
  
  // Copy original file as jpg with suffix '_pdf'
  if(strpos($variables['path'], 'pdf') !== false){
    $variables_new =  _img_ext_convert_pdf_2_jpg($variables['path']);
  }
  
  // Copy original file as jpg with suffix '_svg'
  elseif(strpos($variables['path'], 'svg') !== false){
    $variables_new =  _img_ext_convert_svg_2_jpg($variables['path']);
  }
  
  // Copy original file as jpg with suffix '_svg'
  elseif(strpos($variables['path'], 'eps') !== false){
    $variables_new =  _img_ext_convert_eps_2_jpg($variables['path']);
  }
  
  // Copy original file as jpg with suffix '_tiff'
  elseif(strpos($variables['path'], '.tif') !== false){
    $variables_new =  _img_ext_convert_tiff_2_jpg($variables['path']);
  }
  
  return $variables_new;
}

/**
 * Prepare the file system for PDF/SVG/EPS preview image creation
 *
 */
function _img_ext_prepare_filesystem($imgext_dir) {
  if (!file_prepare_directory($imgext_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    drupal_set_message(t('Error pepraring directory %dir', array('%dir' => $imgext_dir)), 'error');
  }
}

/**
 * Converts pdf to jpg in folder imgext and calls the new file,with name and size
 * 
 * @param type $filepath
 * @return boolean
 */
function _img_ext_convert_pdf_2_jpg($filepath){
  $details = FALSE;
  if (!is_file($filepath) && !is_uploaded_file($filepath)) {
    return $details;
  }
  preg_match('/(?<=\/\/).*(?=\.pdf)/', $filepath, $matches);
  $filename_without_pdf_extension = img_ext_clear_string($matches[0]);
  
  if(variable_get('img_ext_prefer_pdftoppm') == 1){
    shell_exec(variable_get("img_ext_pdftoppm"). ' -jpeg -f 1 -singlefile '. drupal_realpath($filepath).' ' 
    . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf" ));
  }
  else{
    shell_exec(variable_get("imagemagick_convert").' '. drupal_realpath($filepath).'[0] -scale 1024 ' 
      . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf.jpg" ));
  }
  
  $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf.jpg" );
  $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf.jpg";
  
  // Creates new image path and size for variables.
  $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
  
  return $details;
}

/**
 * Converts svg to jpg in folder imgext and calls the new file,with name and size
 * 
 * @param type $filepath
 * @return boolean
 */
function _img_ext_convert_svg_2_jpg($filepath){
  $details = FALSE;
  if (!is_file($filepath) && !is_uploaded_file($filepath)) {
    return $details;
  }
  preg_match('/(?<=\/\/).*(?=\.svg)/', $filepath, $matches);
  $filename_without_svg_extension = img_ext_clear_string($matches[0]);

  /*shell_exec('/usr/bin/pdftoppm -jpeg -f 1 -singlefile '. drupal_realpath($filepath).' ' 
      . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf" ));*/
  shell_exec(variable_get("imagemagick_convert").' '. drupal_realpath($filepath).'[0] -scale 1024 ' 
      . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_svg_extension" . "_svg.jpg" ));
  
  $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_svg_extension" . "_svg.jpg" );
  $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_svg_extension" . "_svg.jpg";
  
  // Creates new image path and size for variables.
  $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
  
  return $details;
}

/**
 * Converts eps to jpg in folder imgext and calls the new file,with name and size
 * 
 * @param type $filepath
 * @return boolean
 */
function _img_ext_convert_eps_2_jpg($filepath){
  $details = FALSE;
  if (!is_file($filepath) && !is_uploaded_file($filepath)) {
    return $details;
  }
  preg_match('/(?<=\/\/).*(?=\.eps)/', $filepath, $matches);
  $filename_without_eps_extension = img_ext_clear_string($matches[0]);

  shell_exec(variable_get("imagemagick_convert").' '. drupal_realpath($filepath).'[0] ' 
      . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_eps_extension" . "_eps.jpg" ));
  
  $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_eps_extension" . "_eps.jpg" );
  $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_eps_extension" . "_eps.jpg";
  
  // Creates new image path and size for variables.
  $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
  
  return $details;
}

/**
 * Converts tiff to jpg in folder imgext and calls the new file,with name and size
 * 
 * @param type $filepath
 * @return boolean
 */
function _img_ext_convert_tiff_2_jpg($filepath){
  $details = FALSE;
  if (!is_file($filepath) && !is_uploaded_file($filepath)) {
    return $details;
  }
  preg_match('/(?<=\/\/).*(?=\.tif)/', $filepath, $matches);
  $filename_without_tiff_extension = img_ext_clear_string($matches[0]);

  /*shell_exec('/usr/bin/pdftoppm -jpeg -f 1 -singlefile '. drupal_realpath($filepath).' ' 
      . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_pdf_extension" . "_pdf" ));*/
  shell_exec(variable_get("imagemagick_convert").' '. drupal_realpath($filepath).'[0] ' 
      . drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_tiff_extension" . "_tiff.jpg" ));
  
  $image_new_path = drupal_realpath(file_default_scheme() . '://' . 'imgext/'."$filename_without_tiff_extension" . "_tiff.jpg" );
  $new_image_scheme = file_default_scheme() . '://' . 'imgext/'."$filename_without_tiff_extension" . "_tiff.jpg";
  
  // Creates new image path and size for variables.
  $details = _img_ext_imagemagick_identify($image_new_path, $new_image_scheme);
  
  return $details;
}

/**
 * Identifies with imagemagick path and size
 * 
 * @param type $image_new_path
 * @param type $new_image_scheme
 * @return type
 */
function _img_ext_imagemagick_identify($image_new_path,$new_image_scheme){
  $vars = $size = array();
  $size = shell_exec(variable_get("img_ext_identify"). ' -format \'%wx%h\' '.$image_new_path);
  if($size){
    list($width,$height) = explode('x', $size);
    $vars['width'] = $width;
    $vars['height'] = $height;
  }
  $vars['path'] = $new_image_scheme;
  
  
  return $vars;
}

/**
 * Gets details about a pdf, svg, eps, tif(f) per imagemagick.
 *
 * Drupal supports GIF, JPG and PNG file formats when used with the GD
 * toolkit, and may support others, depending on which toolkits are
 * installed. But GD toolkit cannot do the same as imagemagick!
 *
 * @param $filepath
 *   String specifying the path of the image file.
 *
 * @return
 *   FALSE, if the file could not be found or is not an image. Otherwise, a
 *   keyed array containing information about the image:
 *   - "width": Width, in pixels.
 *   - "height": Height, in pixels.
 */
function img_ext_get_pdf_validation($filepath,$size) {  
  $errors = array();
  
  if (!is_file($filepath) && !is_uploaded_file($filepath)) {
    return FALSE;
  }
  
  // Here Imagemagick identifies the size of the file.
  $image = new Imagick(drupal_realpath($filepath)); 
  $ident = $image->identifyImage();
  $ident_width = $ident['geometry']['width'];
  $ident_height = $ident['geometry']['height'];
  //dpm($_SESSION);
  // Check that the doc is smaller than the given dimensions.
  if (array_key_exists('max_resolution_width', $size)) {
    
    if (isset($_SESSION) && array_key_exists('IMG EXT MAX DOC', $_SESSION) &&  $_SESSION['IMG EXT MAX DOC'] == 1){
      
      if ($ident_width > $size['max_resolution_width'] || $ident_height > $size['max_resolution_height']) {
        $errors[] = t('The image is too large; the maximum dimensions are %dimensions pixels.', 
            array('%dimensions' =>  $size['max_resolution_width'] .' x '. $size['max_resolution_height'] ));
      }
    }
  }
  // Check that it is larger than the given dimensions.
  if (array_key_exists('min_resolution_width', $size)) {

    if (isset($_SESSION)  && array_key_exists('IMG EXT MIN DOC', $_SESSION)  &&  $_SESSION['IMG EXT MIN DOC'] == 1){
      
      if ($ident_width < $size['min_resolution_width'] || $ident_height < $size['min_resolution_height'] ) {
        $errors[] = t('The image is too small; the minimum dimensions are %dimensions pixels.',
            array('%dimensions' => $size['min_resolution_width'] .' x '. $size['min_resolution_height'] ));
      }
    }
  }

  return $errors;
}


/**
 * Get details about an image.
 *
 * @param $image
 *   An image object.
 * @return
 *   FALSE, if the file could not be found or is not an image. Otherwise, a
 *   keyed array containing information about the image:
 *   - width: Width in pixels.
 *   - height: Height in pixels.
 *   - extension: Commonly used file extension for the image.
 *   - mime_type: MIME type ('image/jpeg', 'image/gif', 'image/png').
 *
 * @see image_get_info()
 */
function img_ext_imagemagick_get_info(stdClass $image) {
  $details = FALSE;
  $data = getimagesize(drupal_realpath($image->source));

  if (isset($data) && is_array($data)) {
    $extensions = array('1' => 'gif', '2' => 'jpg', '3' => 'png');
    $extension = isset($extensions[$data[2]]) ?  $extensions[$data[2]] : '';
    $details = array(
      'width'     => $data[0],
      'height'    => $data[1],
      'extension' => $extension,
      'mime_type' => $data['mime'],
    );
  }

  return $details;
}

/**
 * Gets Session variable
 * 
 * @param type $session_name
 * @param type $default
 * @return type
 */
function img_ext_session_get($session_name, $default = NULL) {
	if (isset($_SESSION[$session_name])) {
		return $_SESSION[$session_name];
	}
	return $default;
}

/**
 * Sets Session variable
 * 
 * @param type $session_name
 * @param type $session_value
 */
function img_ext_session_set($session_name, $session_value) {
	$_SESSION[$session_name] = $session_value;
}

/**
 * Deletes Session variable
 * 
 * @param type $session_name
 */
function img_ext_session_del($session_name){
  if(isset($_SESSION)){
    if(array_key_exists($session_name, $_SESSION)){
      unset($_SESSION[$session_name]);
    }
  }
}
